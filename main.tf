data "ignition_directory" "config" {
  path = "${var.home}/.config/postgres"
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "bashrc_aliases" {
  path = "${var.home}/.bashrc.d/postgres-aliases.bash"
  content {
    content = templatefile("${path.module}/templates/aliases.bash", {})
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "environment" {
  path = "${var.home}/.config/postgres/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {
      env = var.postgres_environment
    })
  }
  mode = 384
  uid = var.uid
  gid = var.gid
}

data "ignition_directory" "entrypoint" {
  path = "${var.home}/.config/postgres/docker-entrypoint-initdb.d"
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "entrypoint_sql" {
  path = "${var.home}/.config/postgres/docker-entrypoint-initdb.d/databases.sql"
  content {
    content = templatefile("${path.module}/templates/databases.sql", {
      dbs = var.postgres_databases
      env = var.postgres_environment
    })
  }
  mode = 384
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "db_volume_unit" {
  path = "${var.home}/.config/containers/systemd/pgdata${var.postgres_version}.volume"
  content {
    content = templatefile("${path.module}/templates/pgdata.volume", {})
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "pgbackrest_restore_unit" {
  path = "${var.home}/.config/containers/systemd/pgbackrest-restore.container"
  content {
    content = templatefile("${path.module}/templates/pgbackrest-restore.container", {
      environmentFile = data.ignition_file.environment.path
      postgres_version = var.postgres_version
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "pgbackrest_full_backup_unit" {
  path = "${var.home}/.config/systemd/user/pgbackrest-full-backup.service"
  content {
    content = templatefile("${path.module}/templates/pgbackrest-full-backup.service", {
      environmentFile = data.ignition_file.environment.path
      postgres_version = var.postgres_version
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "pgbackrest_full_backup_timer" {
  path = "${var.home}/.config/systemd/user/pgbackrest-full-backup.timer"
  content {
    content = file("${path.module}/templates/pgbackrest-full-backup.timer")
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_link" "pgbackrest_full_backup_timer" {
  path = "${var.home}/.config/systemd/user/timers.target.wants/pgbackrest-full-backup.timer"
  target = data.ignition_file.pgbackrest_full_backup_timer.path
  hard = false
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "pgbackrest_backup_unit" {
  path = "${var.home}/.config/systemd/user/pgbackrest-backup.service"
  content {
    content = templatefile("${path.module}/templates/pgbackrest-backup.service", {
      environmentFile = data.ignition_file.environment.path
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "pgbackrest_backup_timer" {
  path = "${var.home}/.config/systemd/user/pgbackrest-backup.timer"
  content {
    content = file("${path.module}/templates/pgbackrest-backup.timer")
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_link" "pgbackrest_backup_timer" {
  path = "${var.home}/.config/systemd/user/timers.target.wants/pgbackrest-backup.timer"
  target = data.ignition_file.pgbackrest_backup_timer.path
  hard = false
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "db_container_unit" {
  path = "${var.home}/.config/containers/systemd/postgresql${var.postgres_version}.container"
  content {
    content = templatefile("${path.module}/templates/postgresql.container", {
      environmentFile = data.ignition_file.environment.path
      postgres_version = var.postgres_version
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "postgresql_config" {
  path = "${var.home}/.config/postgres/postgresql.conf"
  content {
    content = templatefile("${path.module}/templates/postgresql.local.conf", {
      conf = var.postgres_config
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "postgresql" {
  directories = [
    data.ignition_directory.config.rendered,
    data.ignition_directory.entrypoint.rendered
  ]

  files = [
    data.ignition_file.bashrc_aliases.rendered,
    data.ignition_file.environment.rendered,
    data.ignition_file.db_volume_unit.rendered,
    data.ignition_file.db_container_unit.rendered,
    data.ignition_file.pgbackrest_restore_unit.rendered,
    data.ignition_file.pgbackrest_backup_unit.rendered,
    data.ignition_file.pgbackrest_backup_timer.rendered,
    data.ignition_file.pgbackrest_full_backup_unit.rendered,
    data.ignition_file.pgbackrest_full_backup_timer.rendered,
    data.ignition_file.entrypoint_sql.rendered,
    data.ignition_file.postgresql_config.rendered,
  ]

  links = [
    data.ignition_link.pgbackrest_backup_timer.rendered,
    data.ignition_link.pgbackrest_full_backup_timer.rendered,
  ]
}
