alias psql="podman exec -ti postgresql15 psql"
alias postgres-logs="journalctl --user -flau postgresql15.service -u pgbackrest-restore.service -u pgbackrest-backup.service -u pgbackrest-full-backup.service --since today"
