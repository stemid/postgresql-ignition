ALTER USER ${env.postgres_user} PASSWORD '${env.postgres_password}';

%{ for db in dbs ~}
CREATE USER ${db.username};
CREATE DATABASE ${db.database};
ALTER DATABASE ${db.database} OWNER TO ${db.username};
GRANT ALL PRIVILEGES ON DATABASE ${db.database} TO ${db.username};
ALTER USER ${db.username} PASSWORD '${db.password}';
\c ${db.database};
GRANT ALL ON SCHEMA public TO ${db.username};
%{ for extension in db.extensions ~}
CREATE EXTENSION IF NOT EXISTS ${extension};
%{ endfor ~}
%{ endfor ~}
