variable "home" {
  type = string
}

variable "uid" {
  type = number
}

variable "gid" {
  type = number
}

variable "entrypoint_scripts" {
  type = list(string)
  default = []
}

variable "postgres_environment" {
  type = object({
    postgres_db = string,
    postgres_user = string,
    postgres_password = string,
    pgbackrest_stanza = string,
    pgbackrest_s3_region = string,
    pgbackrest_s3_bucket = string,
    pgbackrest_s3_endpoint = string,
    pgbackrest_s3_key = string,
    pgbackrest_s3_key_secret = string,
    pgbackrest_cipher_pass = string,
    s3_endpoint_url = string
  })
}

variable "postgres_databases" {
  type = list(object({
    database = string,
    username = string,
    password = string,
    extensions = optional(list(string), [])
  }))
}

variable "postgres_version" {
  type = number
  default = 16
}

variable "postgres_config" {
  type = object({
    max_connections = number
    shared_buffers = string
    effective_cache_size = string
    maintenance_work_mem = string
    checkpoint_completion_target = number
    wal_buffers = string
    default_statistics_target = number
    random_page_cost = number
    effective_io_concurrency = number
    work_mem = string
    huge_pages = string
    min_wal_size = string
    max_wal_size = string
  })

  # PGTuner defaults for a 2GB free RAM and 2 CPU core setup.
  default = {
    max_connections = 100
    shared_buffers = "512MB"
    effective_cache_size = "1536MB"
    maintenance_work_mem = "128MB"
    checkpoint_completion_target = 0.9
    wal_buffers = "16MB"
    default_statistics_target = 100
    random_page_cost = 1.1
    effective_io_concurrency = 200
    work_mem = "2621kB"
    huge_pages = "off"
    min_wal_size = "1GB"
    max_wal_size = "4GB"
  }
}
