terraform {
  required_providers {
    ignition = {
      source = "community-terraform-providers/ignition"
      version = "2.2.1"
    }
    template = {
      source = "hashicorp/template"
      version = "2.2.0"
    }
  }
}
